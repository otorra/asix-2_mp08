*Oscar Torra*

*M08: Serveis de Xarxa i Internet*

//Exemple de títol
= Pràctica 2: Pràctica servidor DHCP
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

//Exemple de subtítol

== Instal·lació del servidor DHCP a Farnsworth



//Exemple d'imatge (ha d'existir la carpeta images al mateix directori que el document
.*Captura del contingut del fitxer `/etc/default/isc-dhcp-server`.*
image::captures/cap1.png[]

*Ruta completa a l’arxiu de configuració.*
====
`/etc/dhcp/dhcpd.conf`
====

*Instrucció utilitzada per reiniciar el servidor.*
====
`sudo systemctl restart isc-dhcp-server.service`
====

.*Captura del resultat de la instrucció: `sudo cat /var/log/syslog | grep dhcpd`.*

image::captures/cap4.png[]


*Explica el significat dels missatges de la captura anterior. Què estem veient?*
====
*PENDENT*
====

== Servidor DHCP a la mateixa xarxa

.*Arxiu de configuració complet, esborreu tots els comentaris i feu-lo llegible.*
image::captures/cap5.png[]

== Configuració de la màquina client PC1 (a la mateixa xarxa)

.*Mostrar configuració de xarxa de PC1*
image::captures/cap6.png[]

.*Comanda per demanar una nova IP amb `dhclient`*
image::captures/cap8.png[]

.*Captura de les últimes línies del `syslog` de Farnsworth.*
image::captures/cap9.png[]

.*Captura de pantalla amb la sortida del tcpdump.*
image::captures/cap10.png[]

.*Validar l'adreça ip*
image::captures/cap11.png[]

.*Validar la porta d'enllaç*
image::captures/cap12.png[]

.*Validar servidor dns*
image::captures/cap13.png[]

.*Validar l’arxiu on s’emmagatzema el registre de les cessions DHCP, i trobeu el fragment on s’ha registrat l’operació anterior.*
image::captures/cap14.png[]

.*Validar l’adreça MAC de la targeta enp0s3 des de línia de comandes*
image::captures/cap15.png[]


== Reserva d’adreces IP

.*Captura de la configuració del servei DHCP*
image::captures/cap16.png[]

.*Captura de la instrucció per validar l’adreça IP i del seu resultat.*
image::captures/cap17.png[]

== Grups d’adreces i clients registrats

.*Captura de les modificacions de l’arxiu de configuració.*
image::captures/cap18.png[]

.*Captura del terminal amb la renovació i la IP adquirida del PC1*
image::captures/cap19.png[]

.*Captura del terminal amb la renovació i la IP adquirida del PC2*
image::captures/cap20.png[]

== Configuració de PC1 i PC2 a una xarxa diferent

.*Captura de la nova configuracio*
image::captures/cap21.png[]

.*Comprovació de la IP de PC1*
image::captures/cap22.png[]

.*Comprovació de la IP de PC2*
image::captures/cap23.png[]
== Configuració d’un servidor secundari
.*Instal·lació del NTP a Farnsworth i a Hermes.*
image::captures/cap24.png[]

.*Demostració que el servidor DHCP s’està executant a Hermes.*
image::captures/cap25.png[]

.*Captura de la configuració de failover a Farnsworth.*
image::captures/cap26.png[]

.*Captura de la configuració de failover a Hermes.*
image::captures/cap27.png[]

.*Demostració que el failover funciona.*
image::captures/cap31.png[]






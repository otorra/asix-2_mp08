*Oscar Torra*

*M08: Serveis de Xarxa i Internet*

//Exemple de títol
= Pràctica 3: Pràctica DNS
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

//Exemple de subtítol

== Instal·lació del servidor DNS a Zoidberg

//Exemple d'imatge (ha d'existir la carpeta images al mateix directori que el document
Quina és la IP de louvre.fr? Quina és la IP del servidor DNS que ens ha
respost?

.*Captura del resultat de la consulta DNS.*
image::captures/capt1.png[]

.*Captura de la instrucció que instal·la el servidor DNS.*
image::captures/capt2.png[]

.*Captura del resultat de la instrucció netstat que mostra el servidor DNS funcionant.*
image::captures/capt3.png[]

.*Captura de la execució de systemd-resolve --status*
image::captures/capt4.png[]

Utilitza rndc per activar la depuració del servidor. Posa la depuració
a nivell 3 (com més gran aquest nombre, més informació es guarda al registre.

.*Captura de la instrucció utilitzada.*
image::captures/capt5.png[]

.*Captura de /etc/default/named*
image::captures/capt6.png[]

.*Captura de /etc/bind/named.conf.options*
image::captures/capt7.png[]

Consulta la IP d’un nou domini, per exemple britishmuseum.org.

.*Captura de la instrucció utilitzada i el seu resultat.*
image::captures/capt8.png[]

.*Part del registre del bind corresponent a l’última petició.*
image::captures/capt9.png[]

Bolca el cau del servidor DNS a un fitxer utilitzant rndc i comprova quina informació s’ha
guardat sobre britishmuseum.org.

.*Instrucció per bolcar el cau a un fitxer i contingut obtingut.*
image::captures/capt10.png[]

.*Instrucció per desactivar la depuració del servidor DNS.*
image::captures/capt11.png[]

== Configuració d’un servidor DNS només cache

.*Captura de les modificacions fetes a la configuració.*
image::captures/capt12.png[]

== Configuració d’un servidor DNS forwarding

.*Captura de les modificacions fetes a la configuració.*
image::captures/capt13.png[]

.*Sentències utilitzades i resultats.*
image::captures/capt14.png[]

== Configuració dels clients

.*Comprovació a Zoidberg*
image::captures/capt20.png[]

.*Comprovació a PC1*
image::captures/capt21.png[]

== Creació de la zona interna

.*Fixer /etc/bind/named.conf.local*
image::captures/capt22.png[]

== Configuració de la resolució directa

.*Configuració de la resolució directa.*
image::captures/capt23.png[]

== Configuració de la resolució inversa

.*Configuració de la resolució inversa.*
image::captures/capt24.png[]

== Comprovació de la configuració

.*Comprovació checkzone*
image::captures/capt25.png[]

.*Comprovació resolucio directa 1*
image::captures/capt26.png[]

.*Comprovació resolucio directa 2*
image::captures/capt27.png[]

.*Comprovació resolucio inversa 1*
image::captures/capt28.png[]

.*Comprovació resolucio directa 2*
image::captures/capt29.png[]

.*Comprovació resolucio domini d'net inter*
image::captures/capt30.png[]